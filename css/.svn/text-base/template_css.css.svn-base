@charset "utf-8";
/* CSS Document */

/* css reset */
	html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td {margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;}
	table, tr, th, td { vertical-align: top; }
	ul, ul li { margin: 0; padding: 0; list-style: none }
	blockquote, q {quotes: none;}
	blockquote:before, blockquote:after,q:before, q:after {content: '';content: none;}
	/* remember to highlight inserts somehow! */
	ins {text-decoration: none;}
	del {text-decoration: line-through;}
	/* tables still need 'cellspacing="0"' in the markup */
	table {border-collapse: collapse;border-spacing: 0;}
	th { text-align: left; }
	input, textarea { font-family: sans-serif; font-size: 100% }
	/* IE stretched buttons fix */
	input.submit {width: 0;overflow:visible;}
	input.submit[class] {width: auto;}
/* css reset end */

/* elements */
body {font-size: 12px; line-height: 18px; font-family: sans-serif; color: #000; background: #f0f3f5 url(../_img/bg.jpg) repeat-x; text-align: center; }
a {color: #000099; text-decoration: underline;}
a:hover { color: #000; text-decoration: none;}

/* common classes */
.content h1 {font-size: 24px; font-weight: bold; color: #faaa00; margin: 18px 0 18px 0; padding: 0;}
.content h2 {font-size: 20px; font-weight: bold; color: #faaa00; margin: 18px 0 18px 0;}
.content h3 {font-size: 18px; font-weight: bold; color: #000099; margin: 18px 0 18px 0;}
.content h4 {font-size: 14px; font-weight: bold; color: #000099; margin: 18px 0 18px 0;}
.content p {margin: 0 0 10px 0;}
.content a {color: #3879fa;}

.content ul {margin: 20px 0 20px 20px;}
.content ul li ul {margin: 5px 0;}
.content ul li {padding: 0 0 0 18px; background: url(../_img/li-orange.png) no-repeat 0 5px; _background: url(../_img/li-orange.gif) no-repeat 0 5px}
.content ul li ul li {padding: 0 0 0 18px; background: url(../_img/li-green.png) no-repeat 0 5px; _background: url(../_img/li-green.gif) no-repeat 0 5px}

.link {background: url(../_img/pathway.png) no-repeat right center; _background: url(../_img/pathway.gif) no-repeat right center; padding: 0 12px 0 0;}
hr { clear:both; margin:0; padding:0; font-size:1px; height:0px; line-height:1px; overflow:hidden; border:none;}

table.normal {border-top: 1px solid #b7cdfa; border-left: 1px solid #b7cdfa;}
table.normal th, td {padding: 5px 10px; border-right: 1px solid #b7cdfa; border-bottom: 1px solid #b7cdfa;}
table.normal th {background: #6597fa; color: #fff;}
table.normal tr {background: #ebf0fa;}
table.normal .highlight {background: #fafafa;}

.normal-form label { font-weight:bold; line-height: 28px; width:190px; float:left; color:#004799; text-align: right; margin: 0 15px 0 0;}
.normal-form div {margin: 0 0 10px 0;}
.input-field-long {width: 220px; height: 28px; line-height: 28px; background: url(../_img/input-long.png) no-repeat 0 0; margin: 0; padding: 0 5px; border: 0; color: #004799;; font-weight: bold;}
.input-field-long:hover, .input-field-long:focus {background: url(../_img/input-long.png) no-repeat 0 -28px; color: #fff;}
.input-field-short {width: 140px; height: 28px; line-height: 28px; background: url(../_img/input-long.png) no-repeat 0 0; margin: 0; padding: 0 5px; border: 0; color: #004799;; font-weight: bold;}
.input-field-short:hover, .input-field-short:focus {background: url(../_img/input-long.png) no-repeat 0 -28px; color: #fff;}
.normal-button { margin: 0px; padding:0; width:120px; height:28px; background: url("../_img/button.png") no-repeat 0 -28px; color:#fff; font-weight:bold; border: none; cursor: pointer; text-align: center; }
.normal-button:hover { background: url("../_img/button.png") no-repeat 0 0; }

.pagehub {padding: 10px 0 10px 20px; font-weight: bold;text-align: center;}
.pagehub a, .pagehub span {display: block; float: left; width: 20px; line-height: 27px; color: #004799;}
a.pagenav-ico {}
a.pagenav-ico, span.pagenav-ico { font-size: 20px; width: 27px; height: 32px; line-height: 24px; background: url(../_img/pagehub-bg.png) no-repeat 0 0; color: #fff; padding: 0 5px 0 0; text-decoration: none;}
a:hover.pagenav-ico {background: url(../_img/pagehub-bg.png) no-repeat 0 -32px;}

.colored-grey {color: #777;}
.colored-blue {color: #004799;}
.img-left {float: left; margin: 5px 20px 10px 0;}
.clear-decor {clear: both; background: url(../_img/hr.png) repeat-x center; height: 30px; margin: 10px 0;}

.r { text-align: right !important; }
.c { text-align: center !important; }
.l { text-align: left !important; }
.j { text-align: justify !important; }
.cleaner {clear: both; height: 0; overflow: hidden;}
.nodisplay {display: none;}

/* Layout */

.page {
width: 950px;
margin: 0 auto;
text-align: left;
}

    .top {
    position: relative;
    width: 100%;
    height: 109px;
    background: url(../_img/top.png) no-repeat bottom left;
    _background: none;
    _filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='_img/top.png',sizingMethod='crop');
    }
    
        .pathway {
        position: absolute;
        top: 9px;
        left: 6px;
        }
        
        .pathway ul li {
        float: left;
        }
        
        .pathway ul li a {
        padding: 0 10px 0 13px;
        text-decoration: none;
        font-weight: bold;
        font-size: 11px;
        background: url(../_img/pathway.png) no-repeat center left;
        _background: url(../_img/pathway.gif) no-repeat center left;
        }
        
        .pathway ul li a:hover {
        text-decoration: underline;
        }
        
        .pathway ul li .act {
        font-weight: normal;
        }
        
        .nav {
        position: absolute;
        right: 6px;
        top: 0;
        }
        
        .nav ul li {
        float: left;
        height: 24px;
        border-right: 1px solid #000099;
        }
        
        .nav ul li a {
        display: block;
        margin: 9px 12px 0 12px;
        padding: 0 0 0 14px;
        font-size: 11px;
        text-decoration: none;
        }
        
        .nav ul li a:hover {
        text-decoration: underline;
        }
        
        .nav ul li .home {
        background: url(../_img/home.png) no-repeat center left;
        _background: url(../_img/home.gif) no-repeat center left;
        }
        
        .nav ul li .sitemap {
        background: url(../_img/sitemap.png) no-repeat center left;
        _background: url(../_img/sitemap.gif) no-repeat center left;
        }
        
        .nav ul li .contact {
        background: url(../_img/contact.png) no-repeat center left;
        _background: url(../_img/contact.gif) no-repeat center left;
        }

.wrapper { 
background: url(../_img/shadow.png) repeat-y;
height: 100%;
_background: none;
_filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='_img/shadow.png',sizingMethod='scale');
}        
              
    .main {
    width: 710px;
    margin: 0 5px 0 0;
    float: right;
    _display: inline;
    }
        
        .columns ul li {
        position: relative;
        text-align: center;
        float: left;
        background: url(../_img/column-top.png) no-repeat;
        }
        
        .columns ul li h1 {
        position: relative;
        font-size: 18px;
        font-weight: bold;
        color: #6b6b6b;
        text-transform: uppercase;
        margin: 7px 0 31px 0;
        }
         
        .columns ul .column01 {
        width: 239px;
        }
       
        .columns ul .column02 {
        width: 239px;
        background-position: -239px 0;
        }
        
        .columns ul .column03 {
        width: 232px;
        background-position: -478px 0;
        }
        
        .columns ul li ul {
        height: 441px;
        text-align: left;
        background: url(../_img/column-body.jpg) no-repeat;
        }
        
        .columns ul .column02 ul {
        background-position: -239px 0;
        }
        
        .columns ul .column03 ul {
        background-position: -478px 0;
        }
     
        .columns ul li ul li {
        display: inline;
        text-align: left;
        float: none;
        background: url(.png);
        _height: 0;
        _overflow: visible;
        _margin: 0 0 5px 0;
        }
          
        .columns ul li ul li a {
        position: relative;
        font-size: 12px;
        line-height: 14px;
        text-transform: none;
        padding: 0 0 8px 17px;
        margin: 0 22px 0 46px;
        font-weight: bold;
        display: block;
        text-decoration: none;
        }
        
        .columns ul li ul li a:hover {
        text-decoration: underline;
        }
        
        .columns ul .column01 ul li a {
        background: url(../_img/li-orange.png) no-repeat 0 4px;
        _background: url(../_img/li-orange.gif) no-repeat 0 4px;
        }
        
        .columns ul .column02 ul li a {
        background: url(../_img/li-green.png) no-repeat 0 4px;
        _background: url(../_img/li-green.gif) no-repeat 0 4px;
        }
        
        .columns ul .column03 ul li a {
        background: url(../_img/li-blue.png) no-repeat 0 4px;
        _background: url(../_img/li-blue.gif) no-repeat 0 4px;
        }
        
        .columns ul li ul li ul {
        background: url(.png);
        }
        
        .columns ul li ul .hover {
        position: absolute;
        left: 0px;
        top: -46px;
        width: 232px;
        height: 231px;
        padding: 307px 0 0 0;
        margin: 0;
        }
        
        .columns ul li ul .effect {
        background: url(../_img/hover.png) no-repeat;
        _background: url(.png);
        }
        
    .content {
    margin: 0 1px;
    padding: 30px;
    height: 100%;
    background: url(../_img/text-bg.png) repeat-x;
    }

    .sidebar {
    width: 221px;
    overflow: hidden;
    margin: 0 0 0 14px;
    float: left;
    overflow: visible;
    background: url(../_img/sidebar.png) no-repeat top right;
    _display: inline;
    }
        
        .sidebar h3 {
        height: 81px;
        width: 210px;
        text-align: center;
        }
        
            .menu {
            width: 190px;
            margin: 0 10px 9px 0;
            padding: 0 10px 0 10px;
            border-top: 2px solid #fabc38;
            border-bottom: 2px solid #fabc38;
            }
            
            .menu ul {
            margin: 4px 0 21px 0;
            }
            
            .menu ul li {
            background: url(../_img/menu-li.png) no-repeat center bottom;
            _height: 0;
            _overflow: visible;
            }
            
            .menu ul li a {
            display: block;
            color: #004799;
            font-size: 13px;
            text-decoration: none;
            font-weight: bold;
            padding: 4px 0 12px 20px;
            line-height: 16px;
            background: url(../_img/menu-bg.png) no-repeat 0 8px;
            _height: 0;
            _overflow: visible;
            }
    
            .menu ul li a:hover {
            text-decoration: underline;
            background: url(../_img/menu-bg.png) no-repeat -190px 8px;
            }
            
            .menu ul li ul {
            margin: 0;
            }
            
            .menu ul li ul li {
            background: #fff;
            }
            
            .menu ul li ul li a {
            font-weight: normal;
            padding: 0 0 8px 20px;
            margin: 0 0 0 20px;
            background: #fff url(../_img/menu-bg.png) no-repeat -190px 4px;
            }
            
            .menu ul li ul li a:hover {
            background: #fff url(../_img/menu-bg.png) no-repeat 0 4px;
            }
            
            .menu ul li ul li ul li a {
            margin: 0 0 0 40px;
            }
        
        .news {
        width: 190px;
        font-size: 11px;
        line-height: 16px;
        margin: 8px 0 40px 0;
        color: #000099;
        }
        
        .news ul li {
        margin: 0 0 12px 0;
        }
        
        .news .date {
        color: #3879fa;
        font-weight: bold;
        margin: 0 4px 0 0;
        float: left;
        _display: inline;
        }
        
        .news h4 {
        display: inline;
        }
        
        .news h4 a {
        color: #ff9000;
        font-weight: bold;
        }

.footer {
position: relative;
height: 95px;
margin: 0 0 49px 0;
background: url(../_img/footer.png) no-repeat;
_background: none;
_filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='_img/footer.png',sizingMethod='crop');
}

    .footer p {
    position: absolute;
    top: 44px;
    right: 34px;
    font-size: 11px;
    font-weight: bold;
    color: #004799;
    }