jQuery.noConflict();
jQuery(document).ready(function (){

  /* slideshow */
  jQuery.easing.custom = function (x, t, b, c, d) {
  	var s = 1.70158; 
  	if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
  	return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
  }
  jQuery('.mod_custom-slideshow-header .scrollable').scrollable({easing: 'custom', speed: 900, circular: true}).autoscroll({interval:5000});
  jQuery('.mod_custom-slideshow-bottom .scrollable').scrollable({easing: 'custom', speed: 900, circular: true});

  /* first-child */
  jQuery('.mod_newsflash article:first').addClass('first-child');
  
  /* tabs */
	jQuery('#eshop-detail menu.tabs').tabs('#eshop-detail div.panes > div');

});



